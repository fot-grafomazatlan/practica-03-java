package com.example.pcalculadora_gian_java;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

        public class MainActivity extends AppCompatActivity {
            private EditText txtUser;
            private EditText txtPass;
            public static final String EXTRA_MESSAGE = "com.example.MainActivity.MESSAGE";

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);
                txtUser = findViewById(R.id.txtUsuario);
                txtPass = findViewById(R.id.txtPass);
            }

            public void ingresar(View v) {
                txtUser = findViewById(R.id.txtUsuario);
                txtPass = findViewById(R.id.txtPass);

                String strUsuario = getResources().getString(R.string.usuario);

                if (txtUser.getText().toString().equals(strUsuario) && txtPass.getText().toString().equals("12345")) {
                    Intent intent = new Intent(this, CalculadoraActivity.class);
                    String message = txtUser.getText().toString();
                    intent.putExtra(EXTRA_MESSAGE, message);
                    startActivity(intent);
                    txtUser.setText("");
                    txtPass.setText("");
                } else {
                    Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
                }
            }

            public void salir(View v) {
                AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
                confirmar.setTitle("Calculadora");
                confirmar.setMessage("Quieres salir?");
                confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        finish();
                    }
                });
                confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        // Lógica para el botón "Cancelar"
                    }
                });
                confirmar.show();
            }
        }


