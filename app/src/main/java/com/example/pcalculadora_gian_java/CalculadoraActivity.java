package com.example.pcalculadora_gian_java;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {
    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtUno;
    private EditText txtDos;
    private Calculadora calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComp();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String usuario = extras.getString(MainActivity.EXTRA_MESSAGE);
            lblUsuario.setText(usuario);
        }
    }

    private void iniciarComp() {
        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);
        txtUno = findViewById(R.id.txtNum1);
        txtDos = findViewById(R.id.txtNum2);

        calculadora = new Calculadora(0, 0);
    }

    private boolean vacio() {
        return txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty();
    }

    public void sumar(View v) {
        if (vacio()) {
            Toast.makeText(this, "Campos incorrectos", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.suma();
            lblResultado.setText("Resultado: " + total);
        }
    }

    public void resta(View v) {
        if (vacio()) {
            Toast.makeText(this, "Campos incorrectos", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.resta();
            lblResultado.setText("Resultado: " + total);
        }
    }

    public void multi(View v) {
        if (vacio()) {
            Toast.makeText(this, "Campos incorrectos", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.multiplicacion();
            lblResultado.setText("Resultado: " + total);
        }
    }

    public void div(View v) {
        if (vacio()) {
            Toast.makeText(this, "Campos incorrectos", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.division();
            lblResultado.setText("Resultado: " + total);
        }
    }

    public void limpiar(View v) {
        lblResultado.setText("Resultado");
        txtUno.setText("");
        txtDos.setText("");
    }

    public void regresar(View v) {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar a la página principal?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // Lógica para el botón "Cancelar"
            }
        });
        confirmar.show();
    }
}

